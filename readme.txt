=== Timber ===
Contributors: Sushant, Purnima, Dharma, Anup
Tags: wordpress, starter, timber, twig
Requires at least: 3.7
Stable tag: 0.0.0
PHP version: 5.3.0 or greater

== Changelog ==

= 0.0.1 =
* Boostrap feature updated to register custom post type and taxonomy
* Wordpress udpated
* Plugins updated
* Timber plugin replaced with original one
* Starter theme folder added
* Bower support added in starter theme
* In function.php, ajax sample provided

= Support? =
Leave a [Gitlab issue](https://gitlab.com/relizont/wpbase/issues) and I'll holler back.