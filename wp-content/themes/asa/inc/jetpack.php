<?php
/**
 * Jetpack Compatibility File.
 *
 * @link https://jetpack.me/
 *
 * @package ASA
 */

/**
 * Add theme support for Infinite Scroll.
 * See: https://jetpack.me/support/infinite-scroll/
 */
function asa_jetpack_setup() {
	add_theme_support( 'infinite-scroll', array(
		'container' => 'main',
		'render'    => 'asa_infinite_scroll_render',
		'footer'    => 'page',
	) );
} // end function asa_jetpack_setup
add_action( 'after_setup_theme', 'asa_jetpack_setup' );

/**
 * Custom render function for Infinite Scroll.
 */
function asa_infinite_scroll_render() {
	while ( have_posts() ) {
		the_post();
		get_template_part( 'template-parts/content', get_post_format() );
	}
} // end function asa_infinite_scroll_render
