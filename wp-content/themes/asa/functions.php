<?php

//code to make extra page setting (group) for custom field
function my_acf_options_page_settings( $settings )

{
 $settings['title'] = 'ASA Settings';
 $settings['pages'] = array('Header','Footer' ,'General');
 return $settings;
}

add_filter('acf/options_page/settings', 'my_acf_options_page_settings');