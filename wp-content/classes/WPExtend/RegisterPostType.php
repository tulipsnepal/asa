<?php

namespace WPPlugins\WPExtend;

class RegisterPostType{

	public $post_type;
	public $args = array();
	public $labels = array();

	public function __construct($post_type,$args=array(),$labels=array())
	{
		$this->post_type = $post_type;

		$default_args = array(
			'public' => true,
			'supports' => array('title','editor','thumbnail')
		);

		$required_labels = array(
			'singular_name'	=>	ucwords($this->post_type),
			'plural_name'	=>	ucwords($this->post_type)
		);

		$this->args = $args + $default_args;
		$this->labels = $labels + $required_labels;

		$this->args['labels'] = $this->labels + $this->default_labels();

		add_action('init',array($this,"register"));
	}

	public function register()
	{
		register_post_type($this->post_type,$this->args);
	}

	public function default_labels()
	{
		return array(
			'name'                => $this->labels['plural_name'],
			'singular_name'       => $this->labels['singular_name'],
			'add_new'             => 'Add New '. $this->labels['singular_name'],
			'add_new_item'        => 'Add New '. $this->labels['singular_name'],
			'edit'           	  => 'Edit',
			'edit_item'           => 'Edit '.$this->labels['singular_name'],
			'new_item'            => 'New '.$this->labels['singular_name'],
			'view'           	  => 'View '.$this->labels['singular_name'].' Page',
			'view_item'           => 'View '.$this->labels['singular_name'],
			'search_items'        => 'Search '.$this->labels['plural_name'],
			'not_found'           => 'No matching '.strtolower($this->labels['plural_name']).' found',
			'not_found_in_trash'  => 'No '.strtolower($this->labels['plural_name']).' found in Trash',
			'parent_item_colon'   => 'Parent '.$this->labels['singular_name'],
			'menu_name'           => $this->labels['plural_name']
		);
	}

}