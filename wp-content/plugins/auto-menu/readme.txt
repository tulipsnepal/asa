=== Auto Menu ===
Contributors: Santosh KC, Jamo Web Creations 
Tags: admin, menu, submenu, subpages, child pages
Requires at least: 3.0
Tested up to: 3.3.2
Stable tag: 0.1

By default, WordPress can only automatically add new top-level pages to your custom menu and delete the page from the menu only when the page is removed permanently. 
With this plugin, new child pages will be automatically added to your custom menu just like main pages. 
The pages are also removed from the menu when they are trashed or saved as draft or pending. 
Inspired By : Jamo Web Creations (http://jamocreations.com/)

== Description ==

By default, the menu system in WordPress only allows you to “Automatically add new top-level pages”, and does not offer the same functionality for new child pages. Auto Menu solves that problem.

When enabled, Auto Menu will do its magic whenever you publish a new child page. If the new page’s parent is placed in a custom menu, then the new page will be added to that menu too (one level below its parent). Should you wish to reorder or delete the new page from the menu, you can simply do so on the admin's Menus screen.

The limitaion of the default wordpress is that the menu is not changed even when the page is trashed or saved as draft or pending. With this plugin the page is removed autmoatically from the menu when the page is trashed or saved as draft or pending.

The plugin does not have any settings. 

== Installation ==

The easiest way to install this plugin is via the admin's Plugins screen.

Alternatively:

1. Upload the `auto-submenu` directory to the `/wp-content/plugins/` directory.
1. Activate the plugin through the 'Plugins' menu in WordPress.

== Screenshots ==

1. Publish a new child page.
2. And voila, the new child page has been added automatically to your custom navigation menu.

== Changelog ==

= 0.1 =
* First release.
