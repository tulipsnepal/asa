<?php

/*
Plugin Name: Auto Menu Addition and Removal
Plugin URI: http://tulipstechnologies.com
Description: By default, WordPress can only automatically add new top-level pages to your custom menu and delete the page from the menu only when the page is removed permanently. With this plugin, new  pages will be automatically added to your custom menu just like main pages. The pages are also removed from the menu when they are trashed or saved as draft or pending. Inspired By : Jamo Web Creations (http://jamocreations.com/)
Version: 0.1
Author: Rajiv Khadka
Author URI: http://tulipstechnologies.com/
License: GPL2
*/

/*  Copyright 2012  Jamo Web Creations  (email : hello@jamocreations.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as 
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

class AutoMenu {
	
	/**
	 * Constructor
	 */
	function __construct() {
		add_action( 'publish_page', array( &$this, 'on_publish_page' ) );
		add_action( 'wp_trash_post', array( &$this, 'on_page_trash' ) );
		add_action( 'publish_to_draft', array( &$this, 'on_page_draft' ) );
		add_action( 'publish_to_pending', array( &$this, 'on_page_pending' ) );
	}
	
	/**
	 * When publishing a new child page, add it to the appropriate custom menu.
	 */
	function on_publish_page( $post_id ) {
		//trigger_error('test');
		// Theme supports custom menus?
		if ( ! current_theme_supports( 'menus' ) ) {
			return;
		}
		
		// Published page has parent?
		$post = get_post( $post_id );
		if ( ! $post->post_parent ) {
			return;
		}
		
		// Get menus with auto_add enabled
		$auto_add = get_option( 'nav_menu_options' );
		if ( empty( $auto_add ) || ! is_array( $auto_add ) || ! isset( $auto_add['auto_add'] ) ) {
			return;
		}
		$auto_add = $auto_add['auto_add'];
		if ( empty( $auto_add ) || ! is_array( $auto_add ) ) {
			return;
		}
		
		// Loop through the menus to find page parent
		foreach ( $auto_add as $menu_id ) {
			$menu_parent = NULL;
			$menu_items = wp_get_nav_menu_items( $menu_id, array( 'post_status' => 'publish' ) );
			if ( ! is_array( $menu_items ) ) {
				continue;
			}
			foreach ( $menu_items as $menu_item ) {
				// Item already in menu?
				if ( $menu_item->object_id == $post->ID ) {
					return;
				}
				if ( $menu_item->object_id == $post->post_parent ) {
					$menu_parent = $menu_item;
				}
			}
			// Add new item
			if ( $menu_parent ) {
				wp_update_nav_menu_item( $menu_id, 0, array(
					'menu-item-object-id' => $post->ID,
					'menu-item-object' => $post->post_type,
					'menu-item-parent-id' => $menu_parent->ID,
					'menu-item-type' => 'post_type',
					'menu-item-status' => 'publish'
				) );
				return;
			}
		}
	}

	//remove page from menu when it is saved to pending
	function on_page_pending($post)
	{
		$this->on_page_trash($post->ID);
	}
	
	//remove page from menu when it is saved as draft
	function on_page_draft($post)
	{
		$this->on_page_trash($post->ID);
	}
	
	
	//remove page from menu when it is sent to trash
	function on_page_trash ($post_id)
	{
		$post_id = (int) $post_id;
		$post = get_post($post_id);
		
		// Get menus with auto_add enabled
		$auto_add = get_option( 'nav_menu_options' );
		if ( empty( $auto_add ) || ! is_array( $auto_add ) || ! isset( $auto_add['auto_add'] ) ) {
			return;
		}
		$auto_add = $auto_add['auto_add'];
		if ( empty( $auto_add ) || ! is_array( $auto_add ) ) {
			return;
		}
		
		// Loop through the menus to find page
		foreach ( $auto_add as $menu_id ) {
			$menu_parent = NULL;
			$menu_items = wp_get_nav_menu_items( $menu_id, array( 'post_status' => 'publish' ) );
			if ( ! is_array( $menu_items ) ) {
				continue;
			}
			foreach ( $menu_items as $menu_item ) {
				// Item exists in menu. so need to delete from the menu
				if ( $menu_item->object_id == $post->ID) {
					/*$object_id = $menu_item->object_id;
					
					$menu_item_ids = wp_get_associated_nav_menu_items( $object_id, 'post_type' );
					
					foreach( (array) $menu_item_ids as $menu_item_id ) {*/
						$top_level_menu_ids= $menu_item->ID; //parent menu item id assgined
						wp_delete_post( $menu_item->ID, true ); //remove parent menu from menu item
						foreach ($menu_items as $menu_item) {
							if($menu_item->menu_item_parent == $top_level_menu_ids){
								wp_delete_post( $menu_item->ID, true ); //remove respective child menu from menu item
							}
						}
					//}
				}
				
			}
			
		}
		
		
	}
}

$auto_menu = new AutoMenu();


