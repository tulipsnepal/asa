<?php

/**
 * Plugin Name: App Bootstrap
 * Depends: Classes
 */

add_action('plugins_loaded',function(){

	$quotes = new WPPlugins\WPExtend\RegisterPostType('books',array(
			'singular_name'=>"book",
			'supports'=>array(
				'editor','title','custom_fields','thumbnail'
			)
		));

	$quotes = new WPPlugins\WPExtend\RegisterPostType('slider',array(
			'singular_name'=>"slider",
			'supports'=>array(
				'editor','title','custom_fields','thumbnail'
			)
		));

	$quotes = new WPPlugins\WPExtend\RegisterPostType('Pressbilder Post',array(
			'singular_name'=>"pressbilder post",
			'supports'=>array(
				'editor','title','custom_fields'
			)
		));
	/*$quotes = new WPPlugins\WPExtend\RegisterPostType('cpt_foo',array(
			'singular_name'=>"cpt_foo",
			'supports'=>array(
				'editor','title','custom_fields','thumbnail'
			)
		));

	$quotes = new WPPlugins\WPExtend\RegisterPostType('cpt_foo_hierarchical',array(
			'singular_name'=>"cpt_foo_",
			'hierarchical'=>true,
			'supports'=>array(
				'editor','title','custom_fields','thumbnail','page-attributes'
			)
		));


	$quotes = new WPPlugins\WPExtend\RegisterTaxonomy('taxonomy_foo',array('cpt_foo','cpt_foo_hierarchical'),array(
	    'show_ui' => true,
	    'rewrite' => array( 'slug' => 'taxonomy_foo' ),
	));*/

});