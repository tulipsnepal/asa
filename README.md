# WPBase

WPBase is a Tulips WordPress stack that helps you get started with the best development tools and project structure.

## Features

* Better folder structure
* Package management with [Bower](http://bower.io/)
* Autoloader for mu-plugins (use regular plugins as mu-plugins)

## Requirements

* PHP >= 5.3
* Bower - [Install](http://bower.io/)
* Bower requires [Node and npm](https://nodejs.org/) and [Git](https://git-scm.com/)

## Installation

1. Clone the git repo - `git clone https://gitlab.com/sushantmaharjan/wpbase.git` or `git clone git@gitlab.com:sushantmaharjan/wpbase.git`
2. Go to theme folder `starter`  and run bower `bower install` to install all frontend dependencies

## Deploys

Deployment method coming soon.

## Documentation

* [Folder structure](https://github.com/roots/bedrock/wiki/Folder-structure)
* [Configuration files](https://github.com/roots/bedrock/wiki/Configuration-files)
* [Environment variables](https://github.com/roots/bedrock/wiki/Environment-variables)
* [Composer](https://github.com/roots/bedrock/wiki/Composer)
* [wp-cron](https://github.com/roots/bedrock/wiki/wp-cron)
* [mu-plugins autoloader](https://github.com/roots/bedrock/wiki/mu-plugins-autoloader)

## Contributing

Contributions are welcome from everyone.
Clone or fork repo and send pull/merge request.

## Community

Keep track of development and community news.

* Read to the [Wordpress Codex](https://codex.wordpress.org/)
* Follow documentation [Timber](https://github.com/jarednova/timber/wiki/WP-Integration)
* Follow documentation [ACF](http://www.advancedcustomfields.com/resources)
* Read and subscribe to the [Trello board](https://trello.com/b/mksYokhO/work-in-tech)
